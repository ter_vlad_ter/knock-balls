﻿using Levels;
using UnityEngine;
using UnityEngine.Events;

public class TriggerDestroyZone : MonoBehaviour
{
    [SerializeField] private UnityEvent gameBlockDeactivated;

    private void OnTriggerEnter(Collider other)
    {
        GameBlock gameBlock;
        if (other.TryGetComponent(out gameBlock))
        {
            gameBlockDeactivated?.Invoke();
            gameBlock.Delete(5);
        }
    }
}