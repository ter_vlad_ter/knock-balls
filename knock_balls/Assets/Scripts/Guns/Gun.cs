﻿using Levels;
using UnityEngine;
using UnityEngine.Events;

public class Gun : MonoBehaviour
{
    [SerializeField] private UnityEvent onShot;
    
    private PointRotation _rotation;
    private PointShot _shot;


    private void Start()
    {
        _rotation = GetComponentInChildren<PointRotation>();
        _shot = GetComponentInChildren<PointShot>();
    }
    
    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && MainController.NumBullets > 0)
        {
            _rotation.Rotor();
            _shot.Shoot();
            onShot?.Invoke();
        }
    }
}
