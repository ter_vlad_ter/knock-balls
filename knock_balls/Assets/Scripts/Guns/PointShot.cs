﻿using System;
using Guns;
using UnityEngine;

public class PointShot : MonoBehaviour
{
    
    private Bullet _bullet;
    private ContainerBullets _containerBullets;

    private void Start()
    {
        _bullet = Loader.LoadBullet(1);
        _containerBullets = FindObjectOfType<ContainerBullets>();
    }

    public void Shoot()
    {
        transform.rotation = transform.LookOnCursor();
        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles + new Vector3(-4, 0, 0));
        
        GameObject bullet = Instantiate(_bullet.gameObject, transform.position, transform.rotation, _containerBullets.transform);
        Rigidbody rigidbody = bullet.GetComponent<Rigidbody>();
        rigidbody.AddForce(rigidbody.transform.forward * 200, ForceMode.Impulse);
    }
}
