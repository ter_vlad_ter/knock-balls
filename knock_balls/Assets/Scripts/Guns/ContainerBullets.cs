﻿using System.Collections.Generic;
using Guns;
using UnityEngine;
using UnityEngine.Events;

public class ContainerBullets : MonoBehaviour
{
    private List<Bullet> _bullets = new List<Bullet>();

    public static ContainerBullets Instance;
    
    [SerializeField] private UnityEvent onOutBullet;
    
    private void Awake()
    {
        Instance = this;
    }

    public void AddBullet(Bullet bullet)
    {
        _bullets.Add(bullet);
    }

    public void RemoveBullet(Bullet bullet)
    {
        _bullets.Remove(bullet);
        Debug.Log("Remove" + " count " + _bullets.Count);
        if (_bullets.Count == 0)
        {
            onOutBullet?.Invoke();
        }
    }
}
