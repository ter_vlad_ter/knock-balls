﻿using System;
using UnityEngine;

namespace Guns
{
    public class Bullet : MonoBehaviour
    {
        void Start()
        {
            ContainerBullets.Instance?.AddBullet(this);
            Destroy(gameObject, 5);
            
        }

        private void OnDestroy()
        {
            ContainerBullets.Instance?.RemoveBullet(this);
        }
    }
}
