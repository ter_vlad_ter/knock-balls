﻿using UnityEngine;

public class PointRotation : MonoBehaviour
{
    [Range(0, 40)] [SerializeField] private float maxAngleY = 20;
    [Range(-40, 0)] [SerializeField] private float minAngleY = -20;

    [Range(0, 40)] [SerializeField] private float maxAngleX = 5;
    [Range(-40, 0)] [SerializeField] private float minAngleX = -13;


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(transform.position, 0.2f);
    }
    
    public void Rotor()
    {
        transform.rotation = transform.LookOnCursor()
            .ClampAngle(AngleTool.Axis.Y, maxAngleY, minAngleY)
            .ClampAngle(AngleTool.Axis.X, maxAngleX, minAngleX);
    }
}


public static class AngleTool
{
    public enum Axis
    {
        X,
        Y,
        Z
    }

    public static Quaternion ClampAngle(this Quaternion targetRotation, Axis axisType, float max, float min)
    {
        Vector3 eulerAngle = targetRotation.eulerAngles;

        if (axisType == Axis.X)
        {
            eulerAngle = ChangeStartPoint(eulerAngle, new Vector3(-360, 0, 0));
            var angleValue = Mathf.Clamp(eulerAngle.GetValueByType(axisType), min, max);
            targetRotation = Quaternion.Euler(angleValue, eulerAngle.y, eulerAngle.z);
        }
        else if (axisType == Axis.Y)
        {
            eulerAngle = ChangeStartPoint(eulerAngle, new Vector3(0, -360, 0));
            var angleValue = Mathf.Clamp(eulerAngle.GetValueByType(axisType), min, max);
            targetRotation = Quaternion.Euler(eulerAngle.x, angleValue, eulerAngle.z);
        }
        else
        {
            eulerAngle = ChangeStartPoint(eulerAngle, new Vector3(0, 0, -360));
            var angleValue = Mathf.Clamp(eulerAngle.GetValueByType(axisType), min, max);
            targetRotation = Quaternion.Euler(eulerAngle.x, eulerAngle.y, angleValue);
        }

        Vector3 ChangeStartPoint(Vector3 vector, Vector3 offset)
        {
            if (vector.GetValueByType(axisType) > 180)
            {
                vector += offset;
            }

            return vector;
        }
        return targetRotation;
    }

    public static float GetValueByType(this Vector3 eulerAngle, Axis type)
    {
        if (type == Axis.Y)
            return eulerAngle.y;
        if (type == Axis.X)
            return eulerAngle.x;
        else
            return eulerAngle.z;
    }
}