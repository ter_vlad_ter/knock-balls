﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Levels
{
    public class Level : MonoBehaviour
    {
        public int NumGameBlocks { get; private set; } = 0;

        private void Awake()
        {
            var gameBlocks = GetComponentsInChildren<GameBlock>(true);
            NumGameBlocks = gameBlocks.Length;
        }
    }
}