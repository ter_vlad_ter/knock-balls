﻿using System.Collections.Generic;
using Facebook.Unity;
using UnityEngine;

namespace Levels.Statistics
{
    public class Statistic
    {
        public static void LevelComplete(int indexLevel)
        {
            var tutParams = new Dictionary<string, object>();
            tutParams["Level"] = indexLevel.ToString();

            FB.LogAppEvent(
                "Level Passed",
                parameters: tutParams
            );
        }
    }
}