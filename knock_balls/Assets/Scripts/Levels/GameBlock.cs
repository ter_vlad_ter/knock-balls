﻿using System;
using UnityEngine;

namespace Levels
{
    public class GameBlock : MonoBehaviour
    {
        private void Start()
        {
            var level = GetComponentInParent<Level>();
        }

        public void Delete(float seconds)
        {
            Destroy(gameObject, seconds);
        }
    }
}