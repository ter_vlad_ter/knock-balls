﻿namespace Levels
{
    public interface IListenerLevelUpdate
    {
        void OnUpdateLevel(int numBlocks);
    }
}