﻿using System;
using Levels.Lifecycle;
using Levels.Statistics;
using UnityEngine;

namespace Levels
{
    public class MainController : MonoBehaviour, IListenerLevelUpdate
    {
        public static int CurrentLevel
        {
            get => PlayerPrefs.GetInt("CurrentLevel", 1);
            private set => PlayerPrefs.SetInt("CurrentLevel", value);
        }
        
        public static int NumBullets { get; private set; } = 0;

        private LifeCycleLevel _lifeCycleLevel;
        private ControllerUI _controllerUi;

        private int _numActiveGameBlock = 0;
        private int _numInactivatedGameBlock = 0;


        public static void RestartGame()
        {
            CurrentLevel = 1;
        }
        
        private void Awake()
        {
            _lifeCycleLevel = GetComponentInChildren<LifeCycleLevel>();
        }

        private void Start()
        {
            LoadLevel();
        }

        void IListenerLevelUpdate.OnUpdateLevel(int numBlocks)
        {
            _numActiveGameBlock = numBlocks;
            _numInactivatedGameBlock = 0;
            NumBullets = CurrentLevel * 3;
            ControllerUI.Instance?.UpdateLevel(_numActiveGameBlock, CurrentLevel, NumBullets);
        }

        public void OnDestroyGameBlockListener()
        {
            _numInactivatedGameBlock++;
            ControllerUI.Instance?.SetInactivatedBlocks(_numInactivatedGameBlock);
            if (_numInactivatedGameBlock == _numActiveGameBlock)
            {
                Statistic.LevelComplete(CurrentLevel);
                NextLevel();
            }
        }

        public void ChangeNumBullets()
        {
            NumBullets--;
            ControllerUI.Instance?.SetBullet(NumBullets);
        }

        public void RemovedBullet()
        {
            if (NumBullets <= 0)
            {
                LoadLevel();
            }
        }

        private void LoadLevel()
        {
            _lifeCycleLevel.LoadLevel(this);
        }

        private void NextLevel()
        {
            CurrentLevel++;
            LoadLevel();
        }
        
        
    }
}