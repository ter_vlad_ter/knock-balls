﻿using System;
using UnityEngine;

namespace Levels.Lifecycle
{
    public class LifeCycleLevel : MonoBehaviour
    {
        [SerializeField] private PointSpawnLevel _pointSpawn;
        [SerializeField] private PointDestroyLevel _pointDestroy;
        public Level CurrentLevel { get; private set; }
        [SerializeField] private bool ok;
        private Level OldLevel { get; set; }

        private StateLifecycle _currentState;

        private enum StateLifecycle
        {
            WasCreated,
            WasVisible,
            Playing
        }


        private void OnDrawGizmos()
        {
            void PrintPoint(Component component)
            {
                if (component != null)
                {
                    Gizmos.color = Color.red;
                    Gizmos.DrawSphere(component.transform.position, 0.5f);
                }
            }
            
            PrintPoint(this);
            PrintPoint(_pointSpawn);
            PrintPoint(_pointDestroy);
        }


        public void LoadLevel(IListenerLevelUpdate listenerLevelUpdate)
        {
            OldLevel = CurrentLevel;
            try
            {
                CurrentLevel = Instantiate(Loader.LoadLevel(MainController.CurrentLevel), _pointSpawn.transform.position,
                    Quaternion.identity, transform);
            }
            catch (Exception e)
            {
                MainController.RestartGame();
                LoadLevel(listenerLevelUpdate);
            }
           
            _currentState = StateLifecycle.WasCreated;
            listenerLevelUpdate.OnUpdateLevel(CurrentLevel.NumGameBlocks);
        }


        private void Update()
        {
            if (_currentState == StateLifecycle.WasCreated)
            {
                MoveTo(CurrentLevel, transform.position, () => { _currentState = StateLifecycle.WasVisible; });
                MoveTo(OldLevel, _pointDestroy.transform.position, () => { _currentState = StateLifecycle.WasVisible; });
            }
            else if (_currentState == StateLifecycle.WasVisible)
            {
                DestroyOldLevel();
                _currentState = StateLifecycle.Playing;
            }
        }

        private void MoveTo(Level level, Vector3 nextPosition, Action action)
        {
            if (!level) return;

            Vector3 positionLevel = level.transform.position;

            if (Vector3.Distance(nextPosition, positionLevel) >= 0.1f)
            {
                level.transform.position = Vector3.Lerp(positionLevel, nextPosition, 0.1f);
            }
            else
            {
                action?.Invoke();
            }
        }

        private void DestroyOldLevel()
        {
            if (OldLevel)
                Destroy(OldLevel.gameObject);
            OldLevel = null;
        }
    }
}