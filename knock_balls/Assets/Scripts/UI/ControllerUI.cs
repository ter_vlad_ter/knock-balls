﻿using UI;
using UI.ProgressBar;
using UnityEngine;

public class ControllerUI : MonoBehaviour
{
    public static ControllerUI Instance { get; private set; }

    private ControllerProgressBar _controllerProgressBar;
    private TextNumBullet _textNumBullet;

    private void Awake()
    {
        Instance = this;
        _controllerProgressBar = GetComponentInChildren<ControllerProgressBar>();
        _textNumBullet = GetComponentInChildren<TextNumBullet>();
    }


    public void UpdateLevel(int numBlocks, int currentLevel, int numBullets)
    {
        _controllerProgressBar.SetNewValues(numBlocks, currentLevel);
        _textNumBullet.Text = numBullets.ToString();
    }


    public void SetInactivatedBlocks(int numInactivatedBlocks)
    {
        _controllerProgressBar.UpdateProgress(numInactivatedBlocks);
    }

    public void SetBullet(int numBullets)
    {
        _textNumBullet.Text = numBullets.ToString();
    }
}
