﻿using System;
using UnityEngine;

namespace UI.ProgressBar
{
    public abstract class ViewLevel : MonoBehaviour
    {
        private TmpText _text;

        private void Awake()
        {
            _text = GetComponentInChildren<TmpText>();
            OnAwake();
        }

        protected abstract void OnAwake();

        public string Text
        {
            get => _text.Text;
            set => _text.Text = value;
        }
    }
}