﻿using System;
using TMPro;
using UnityEngine;

namespace UI.ProgressBar
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class TmpText : MonoBehaviour
    {
        public String Text
        {
            get => _text.text;
            set => _text.text = value;
        }

        private TextMeshProUGUI _text;

        private void Awake()
        {
            _text = GetComponent<TextMeshProUGUI>();
        }
    }
}