﻿using UnityEngine;

namespace UI.ProgressBar
{
    public class ControllerProgressBar : MonoBehaviour
    {
        private ViewCurrentLevel _viewCurrent;
        private ViewNextLevel _viewNext;
        
        private SliderProgressBar _sliderProgressBar;

        private int NumBlocks = 100;

        void Awake()
        {
            _viewCurrent = GetComponentInChildren<ViewCurrentLevel>();
            _viewNext = GetComponentInChildren<ViewNextLevel>();
            _sliderProgressBar = GetComponentInChildren<SliderProgressBar>();
        }

        public void SetNewValues(int numBlocks, int currentLevel)
        {
            NumBlocks = numBlocks;
            _sliderProgressBar.SetProgress(0);
            _viewCurrent.Text = currentLevel.ToString();
            _viewNext.Text = currentLevel + 1 + "";
            _viewNext.SetStandardColor();
        }

        public void UpdateProgress(int numInactivatedBlocks)
        {
            float progress = (float)numInactivatedBlocks / NumBlocks;
            _sliderProgressBar.SetProgress(progress);

            if (progress == 1 || NumBlocks - numInactivatedBlocks == 0)
            {
                _viewNext.SetFinalColor();
            }
        }
        
    }
}
