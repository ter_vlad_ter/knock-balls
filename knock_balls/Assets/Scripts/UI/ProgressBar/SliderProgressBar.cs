﻿using UnityEngine;
using UnityEngine.UI;

namespace UI.ProgressBar
{
    [RequireComponent(typeof(Slider))]
    public class SliderProgressBar : MonoBehaviour
    {
        private Slider _slider;

        private void Start()
        {
            _slider = GetComponent<Slider>();
        }

        public void SetProgress(float value)
        {
            if (_slider)
                _slider.value = value;
        }
    }
}