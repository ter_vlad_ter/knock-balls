﻿using UnityEngine;
using UnityEngine.UI;

namespace UI.ProgressBar
{
    public class ViewNextLevel : ViewLevel
    {
        [SerializeField] private Color colorBorder;
        [SerializeField] private Color colorBackground;

        private Color _colorBorderStandard;
        private Color _colorBackgroundStandard;

        private Image imageBorder;
        private Image imageBackground;

        public void SetFinalColor()
        {
            _colorBorderStandard = imageBorder.color;
            _colorBackgroundStandard = imageBackground.color;

            imageBackground.color = colorBackground;
            imageBorder.color = colorBorder;
        }

        public void SetStandardColor()
        {
            imageBackground.color = _colorBackgroundStandard;
            imageBorder.color = _colorBorderStandard;
        }
        
        protected override void OnAwake()
        {
            imageBorder = GetComponentInChildren<Image>();
            imageBackground = GetComponent<Image>();

            _colorBackgroundStandard = GetComponentInChildren<Image>().color;
            _colorBorderStandard = GetComponent<Image>().color;
        }
    }
}