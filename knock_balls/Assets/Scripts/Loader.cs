﻿using System;
using Guns;
using Levels;
using UnityEngine;

    public class Loader
    {
        public static Level LoadLevel(int indexLevel)
        {
            string path = "Levels/Level_";

            var level = Resources.Load<Level>(path + indexLevel);

            if (level)
            {
                return level;
            }
            
            throw new Exception("Не найден уровень " + path + indexLevel);
        }

        public static Bullet LoadBullet(int indexBullet)
        {
            string path = "Bullets/[Bullet]_";

            var bullet = Resources.Load<Bullet>(path + indexBullet);

            if (bullet)
                return bullet;
            
            throw new Exception("Не найдена пуля " + path + indexBullet);
        }
    }
